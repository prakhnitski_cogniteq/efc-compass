<?php

use App\Models\Answer;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMissingJsonExtraToAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $wrongAnswers = Answer::where('question_id', 11)->orWhere('question_id', 13)->get();

        foreach ($wrongAnswers as $answer) {
            DB::table('answers')->where('id', $answer->id)->update([
                'extra' => json_encode(["amount" => (int)$answer->value, "original_amount" => $answer->value])
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('answers', function (Blueprint $table) {
            //
        });
    }
}
