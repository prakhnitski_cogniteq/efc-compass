<?php

Route::middleware('throttle:30,1')->namespace('Auth')->group(function () {
    Route::get('/auth/token', 'LoginController@showLoginForm')->name('login');
    Route::get('/temp_working', function () {
        return view('temp_working');
    })->name('temp_working');
    
    Route::post('/auth/token', 'LoginController@otp');
    Route::post('/auth/login', 'LoginController@login');
    Route::get('/auth/logout', 'LoginController@logout');
});

Route::middleware('auth')->group(function () {
    Route::get('/home', function () {
        return redirect('/');
    });

    Route::get('/', function () {
        return view('survey');
    });

    Route::get('user/exportExcel', 'UserController@exportExcelFile');
    Route::get('user/exportCsv', 'UserController@exportCsvFile');

    Route::prefix('api')->namespace('Api')->group(function () {
        Route::resource('categories', 'CategoryController', ['only' => ['index']]);

        Route::get('organisations', 'OrganisationController@index');
        Route::get('organisation', 'OrganisationController@show');
        Route::get('currencies', 'CurrencyController@index');
        Route::resource('surveys', 'SurveyController', ['only' => ['index', 'show', 'store']]);
        Route::resource('surveys.answers', 'AnswerController', ['only' => ['index', 'store', 'destroy']]);
        Route::resource('surveys.questions', 'QuestionController', ['only' => ['index', 'show', 'store', 'destroy']]);
        Route::resource('surveys.questions.analytics', 'AnalyticsController', ['only' => 'index']);
    });
});
