<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Exports\DataExport;
use App\Models\Organisation;
use App\Models\Question;


class UserController extends Controller
{
    public function exportExcelFile()
    {
    	// if admin give permission for download
    	$status = request()->user()->is_admin;
    	
		if ($status == 1) {
		
        return Excel::download(new DataExport, 'data.xls');
    	}
    	return redirect('/');
    	
    }

    public function exportCsvFile()
    {
    	// if admin give permission for download

    	$id = request()->user()->is_admin;

    	if ($id == 1) {
        return Excel::download(new DataExport, 'data.csv');
        }
    	return redirect('/');
    	
    }
}
