<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\OTPRequest;
use App\Models\Organisation;
use App\Notifications\OTPGenerated;
use Illuminate\Support\MessageBag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function showLoginForm() {

        $domains = Organisation::pluck('email_domain');

        return view('auth.login', ['organisation_domains'=>$domains]);
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function otp(OTPRequest $request)
    {

        $emailAddress = $request->input('email');
        if(stristr($emailAddress, '@')) {
            $errors = new MessageBag();
            $errors->add('email', 'Username is not valid');
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($errors);
        }
        $organisation_domain = $request->input('organisation');
        $emailWithDomain = $emailAddress.'@'.$organisation_domain;
//        $domainName = substr(strrchr($emailAddress, "@"), 1);

        $organisation = Organisation::where('email_domain', $organisation_domain)->first();

        if (!$organisation) {
            $errors = new MessageBag();
            $errors->add('email', 'Organisation not found!');
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($errors);
        }

        $user = User::where('email', $emailWithDomain)->first();

        if (!$user) {
            $user = new User;
            $user->email = $emailWithDomain;
            $user->password = '';
            $user->organisation()->associate($organisation);
            $user->save();
        }

        $otp = $user->generateOTP();

        $user->notify(new OTPGenerated($otp));

        return view('auth.token', ['email' => $emailWithDomain]);
    }

    public function login(Request $request)
    {
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return view('auth.token', ['email' => $request->email]);
    }

    protected function redirectPath()
    {
        return $this->redirectTo;
    }

    public function logout()
    {
        Auth::logout();
        return view('auth.login');
    }
}
