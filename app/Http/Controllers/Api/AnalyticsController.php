<?php

namespace App\Http\Controllers\Api;

use App\Models\CurrencyRate;
use App\Models\Submission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Organisation;
use App\Models\Survey;
use App\Models\Question;
use Auth;

class AnalyticsController extends Controller
{
    protected $organisation;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Survey $survey, Question $question)
    {
        return $this->calculateAnalyticsForQuestionFromSurvey($question, $survey);
    }

    /*
     * PRIVATE
     */
    private function calculateAnalyticsForQuestionFromSurvey($question, $survey)
    {
        switch ($question->type) {
            case Question::TYPE_MULTIPLE_CHOICE:
            case Question::TYPE_SINGLE_CHOICE:
            case Question::TYPE_AMOUNT_RANGE:
                return $this->calculateAnalyticsForChoiceBasedQuestionFromSurvey($question, $survey);
            case Question::TYPE_AMOUNT:
                return $this->calculateAnalyticsForAmountBasedQuestionFromSurvey($question, $survey);
            case Question::TYPE_AMOUNT_CURRENCY:
                return $this->calculateAnalyticsForAmountCurrencyBasedQuestionFromSurvey($question, $survey);
            case Question::TYPE_TRUE_FALSE:
            case Question::TYPE_TRUE_FALSE_CONTEXT:
                return $this->calculateAnalyticsForTrueFalseQuestionFromSurvey($question, $survey);
            case Question::TYPE_ORDER:
                return $this->calculateAnalyticsForOrderBasedQuestionFromSurvey($question, $survey);
            default:
                return [];
        }
    }

    private function calculateAnalyticsForChoiceBasedQuestionFromSurvey($question, $survey)
    {
        $response = [];
        $submissionIds = Submission::whereHas('organisation', function ($query) {
            $query->where('email_domain', '=', 'efc.be');
        })->pluck('id');
        $submissionCount = $survey->answers()
            ->where('question_id', $question->id)
            ->whereNotIn('submission_id', $submissionIds)
            ->distinct('submission_id')
            ->count('submission_id');

        foreach ($question->answerOptions as $answerOption) {
            $timesSelectedCount = $survey->answers()
                ->where('question_id', $question->id)
                ->whereNotIn('submission_id', $submissionIds)
                ->where('value', $answerOption->value)
                ->count();
            
            $response[$answerOption->value] = $timesSelectedCount / $submissionCount;
        }
        return $response;
    }

    private function calculateAnalyticsForAmountBasedQuestionFromSurvey($question, $survey)
    {
        $submissionIds = Submission::whereHas('organisation', function ($query) {
            $query->where('email_domain', '=', 'efc.be');
        })->pluck('id');
        $answers = $survey->answers()
            ->where('question_id', $question->id)
            ->whereNotIn('submission_id', $submissionIds)
            ->get();
       
        $total = 0.0;
        $highest = 0.0;
        foreach ($answers as $answer) {
            $originalCurrency = $answer->extra['original_currency'] ?? 'eur';
            $originalAmount = $answer->extra['amount'];
            $amount = 0;
            if ($originalCurrency !== 'eur') {
                $currencyRate = CurrencyRate::where('code', strtoupper($originalCurrency))->orderBy('year', 'DESC')->first();
                if ($currencyRate) {
                    $amount = $originalAmount / $currencyRate->rate;
                }
            } else {
                $amount = $originalAmount;
            }

            if ($amount > $highest) {
                $highest = $amount ;
            }
            $total += $amount;
        }
        return [
            'highest' => $highest,
            'average' => $total / $answers->count()
        ];
    }

    private function calculateAnalyticsForAmountCurrencyBasedQuestionFromSurvey($question, $survey)
    {
        $response = [];
        $submissionIds = Submission::whereHas('organisation', function ($query) {
            $query->where('email_domain', '=', 'efc.be');
        })->pluck('id');
        $answers = $survey->answers()
            ->where('question_id', $question->id)
            ->whereNotIn('submission_id', $submissionIds)
            ->get();

        $total = 0.0;
        $highest = 0.0;
        foreach ($answers as $answer) {
            if ($answer->extra['amount'] > $highest) {
                $highest = $answer->extra['amount'];
            }
            $total += $answer->extra['amount'];
        }
        return [
            'highest' => $highest,
            'average' => $total / $answers->count()
        ];
    }

    private function calculateAnalyticsForOrderBasedQuestionFromSurvey($question, $survey)
    {
        $response = [];

        $submissionIds = Submission::whereHas('organisation', function ($query) {
            $query->where('email_domain', '=', 'efc.be');
        })->pluck('id');
        // TODO: This should still be grouped per survey round.
        foreach ($question->answerOptions as $answerOption) {
            $sortedAnswersByOrder = $survey->answers()
                ->where('question_id', $question->id)
                ->where('value', $answerOption->value)
                ->whereNotIn('submission_id', $submissionIds)
                ->get()
                ->sortBy(function($answer) {
                    return $answer->extra['order'];
                });

            $medianIndex = (int) floor($sortedAnswersByOrder->count() / 2);

            $response[$answerOption->value] = $sortedAnswersByOrder->get($medianIndex)->extra['order'] + 1;
        }
        return $response;
    }

    private function calculateAnalyticsForTrueFalseQuestionFromSurvey($question, $survey)
    {
        $submissionIds = Submission::whereHas('organisation', function ($query) {
            $query->where('email_domain', '=', 'efc.be');
        })->pluck('id');

        $count = $survey->answers()
            ->where('question_id', $question->id)
            ->whereNotIn('submission_id', $submissionIds)
            ->count();

        $trueCount = $survey->answers()
            ->where('question_id', $question->id)
            ->whereNotIn('submission_id', $submissionIds)
            ->where('value', 'true')
            ->count();

        $falseCount = $survey->answers()
            ->where('question_id', $question->id)
            ->whereNotIn('submission_id', $submissionIds)
            ->where('value', 'false')
            ->count();

        return [
            'true' => $trueCount / $count,
            'false' => $falseCount / $count
        ];
    }
}
