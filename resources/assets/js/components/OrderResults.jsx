import React from "react";
import PropTypes from "prop-types";
import { getSortedAnswers } from '../helpers';

const OrderResults = ({
  question,
  answers,
  analytics,
}) => {
  const sortedAnswers = getSortedAnswers(question, answers)

  return (
    <div>
      {sortedAnswers.map((option, index) => {
        let median;
        if (analytics[option.value]) {
          median = Math.round(analytics[option.value]);
        }

        return (
          <div key={option.id} style={{ marginBottom: "0.2rem", marginTop: "0.8rem" }}>
            <p>
              {index + 1}. {option.text}
            </p>
            { median && (
              <p>
                Median position: {median}
              </p>
            )}
          </div>
        );
      })}
    </div>
  )
};

OrderResults.propTypes = {
  analytics: PropTypes.object.isRequired,
  answers: PropTypes.array.isRequired,
  question: PropTypes.object.isRequired,
};

export default OrderResults;
