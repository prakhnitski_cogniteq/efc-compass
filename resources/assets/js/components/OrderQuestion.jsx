import React, { Component } from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash.debounce';
import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc';
import { getSortedAnswers } from '../helpers';

const SortableItem = SortableElement(({value, number}) =>
  <div className="card" style={{ cursor: 'move', marginBottom: '0.5rem' }}>
    <div className="card-body">
      {number}. {value}
    </div>
  </div>
);

const SortableList = SortableContainer(({items}) => {
  return (
    <ul>
    {items.map((option, index) => (
      <SortableItem
        key={`item-${index}`}
        index={index}
        value={option.text}
        number={index + 1}
      />
    ))}
  </ul>
  );
});

class OrderQuestion extends Component
{
  constructor(props) {
    super(props);
    const { question, answers, storeAnswers } = props;

    this.state = {
      answers: getSortedAnswers(question, answers),
    }

    this.debouncedStore = debounce(storeAnswers, 1000);
  }

  componentWillReceiveProps(nextProps) {
    const { question, answers } = nextProps;
    const optionValues = question.answer_options.map((option) => option.value);

    if (nextProps.answers !== this.props.answers) {
      this.setState({
        answers: answers
          .filter((answer) => optionValues.includes(answer.value))
          .sort((a, b) => (a && a.extra && a.extra.order) > (b && b.extra && b.extra.order))
      });
    }
  }

  render() {
    const { question } = this.props;
    const { answers } = this.state;
    const sortedAnswers = getSortedAnswers(question, answers)

    return (
      <div className="no-selection">
        <SortableList
          items={sortedAnswers}
          onSortEnd={(params) => this.onSortEnd(params)}
        />
      </div>
    )
  }

  onSortEnd({oldIndex, newIndex}) {
    const movedArr = arrayMove(this.state.answers, oldIndex, newIndex);
    const answers = movedArr.map((answer, index) => ({ ...answer, extra: { order: index } }))

    this.setState({
      answers,
    });

    this.debouncedStore(answers)
  }
}

OrderQuestion.propTypes = {
  question: PropTypes.object.isRequired,
  answers: PropTypes.array,
  storeAnswers: PropTypes.func.isRequired
};

export default OrderQuestion;
