import React from 'react';
import Spinner from "react-spinkit";

const Loader = () => (
    <div className="row" style={{ minHeight: "10rem" }}>
        <div className="col align-self-center text-center">
            <div className="d-inline-block">
            <Spinner name="line-scale-pulse-out-rapid" />
            </div>
        </div>
    </div>
)

export default Loader;
