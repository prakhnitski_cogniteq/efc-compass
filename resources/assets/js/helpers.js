export const getSortedAnswers = (question, answers) => {
    const optionValues = question.answer_options.map((option) => option.value);

    let answersToSort = [];
    if (answers && answers.length) {
        answersToSort = answers.filter((answer) => optionValues.includes(answer.value));
    } else {
        answersToSort = question.answer_options.map((option, index) => ({
            question_id: question.id,
            answer_option_id: option.id,
            value: option.value,
            text: option.text,
            extra: {
                order: index
            }
        }));
    }

    return answersToSort.sort((a, b) => (a && a.extra && a.extra.order) - (b && b.extra && b.extra.order));
}
