@extends('layouts.main')

@section('content')
    <div id="app"></div>
@endsection
	<script>
	var permission = "false";
	var status = {!!Auth::user()->is_admin!!};
	if(status == 1)
		permission = "true";
	var excelurl = "{!!url('user/exportExcel')!!}"
	var csvurl = "{!!url('user/exportCsv')!!}"
	</script>
	
@section('scripts')
<script src="{{ mix('js/app.js') }}"></script>
@endsection
