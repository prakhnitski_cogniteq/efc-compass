@extends('layouts.main')

@section('content')
<div class="container vh-100">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-12 l-login text-center">
            <img src="{{ asset('/img/logo-efc.svg') }}" alt="EFC Compass" class="l-login__logo" />
            <h2>Work in progress!</h2>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@endsection