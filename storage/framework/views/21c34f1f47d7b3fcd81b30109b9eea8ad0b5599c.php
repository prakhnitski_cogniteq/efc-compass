<!doctype html>
<html lang="<?php echo e(app()->getLocale()); ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

        <title>EFC Compass</title>

        <link href=<?php echo e(mix('css/app.css')); ?> type="text/css" rel="stylesheet" />
    </head>
    <body>
        <?php echo $__env->yieldContent('content'); ?>
    </body>

    <?php echo $__env->yieldContent('scripts'); ?>
</html>
