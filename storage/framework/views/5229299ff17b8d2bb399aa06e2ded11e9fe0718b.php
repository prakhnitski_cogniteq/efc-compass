<?php $__env->startSection('content'); ?>
    <div class="container vh-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-12 l-login text-center">
                <img src="<?php echo e(asset('/img/logo-efc.svg')); ?>" alt="EFC Compass" class="l-login__logo" />
                <h2>Work in progress!</h2>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>